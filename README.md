# Playing With The Sun Construction Kit Hardware

Project website: [playingwiththesun.org](https://playingwiththesun.org)

**Disclaimer:** 3D previews are a bit funky, since we haven't bothered with getting the right 3D models for the components.. so.. it's gonna look different in real life.. visit the website above to see for yourself.

# Rules for PCB designs:
Protect your inputs, limit your outputs.

# Maximum voltage
The whole ecosystem is designed to be 5V tolerant, but should work okay at lower voltages. Don't go too much above 6V... We don't know what will happen .. yet :)

## Boards:

### [Miller Solarengine](PWTS_designs/miller)
A miller type solar engine using a TC54 "voltage governor" IC. Inspired by the excellent [solarbotics](http://solarbotics.net/library/circuits/se_t1_mse.html).
It stores (solar)energy in a cap and releases it in pulses when there's "enough". (Threshold is at 3.3ish volts at the moment.. i think...)

[<img src="PWTS_designs/miller/previews/V3/SCH.png" width="23%"/>](PWTS_designs/miller/previews/V3/SCH.png)
[<img src="PWTS_designs/miller/previews/V3/PCB.png" width="23%"/>](PWTS_designs/miller/previews/V3/PCB.png)
[<img src="PWTS_designs/miller/previews/V3/3D_Front.jpg" width="23%"/>](PWTS_designs/miller/previews/V3/3D_Front.jpg)
[<img src="PWTS_designs/miller/previews/V3/3D_Back.jpg" width="23%"/>](PWTS_designs/miller/previews/V3/3D_Back.jpg)

### [Power Pack](PWTS_designs/ultra)
A supercapacitor based energy storage solution used to power the constructions when the sun doesn't want to play. Charged with a hand crank, or an exercise bike, or a tiny wind turbine.. and who knows what else? (Even solar panels will do it..)

Mount either a fixed resistor (RL2: 18 ohms seems like the right value at the moment.) or a trimpot (RL1: 0-20 ohm would be reasonable) ..Don't mount both.. that would be silly.

[<img src="PWTS_designs/ultra/previews/V3/SCH.png" width="23%"/>](PWTS_designs/ultra/previews/V3/SCH.png)
[<img src="PWTS_designs/ultra/previews/V3/PCB.png" width="23%"/>](PWTS_designs/ultra/previews/V3/PCB.png)
[<img src="PWTS_designs/ultra/previews/V3/3D_Front.jpg" width="23%"/>](PWTS_designs/ultra/previews/V3/3D_Front.jpg)
[<img src="PWTS_designs/ultra/previews/V3/3D_Back.jpg" width="23%"/>](PWTS_designs/ultra/previews/V3/3D_Back.jpg)

### [Motor](PWTS_designs/motor)
Designed to sort of integrate with [this DC motor](https://www.adafruit.com/product/2941), and live off of whatever energy is being fed to it. Also has a switch to change motor direction.

[<img src="PWTS_designs/motor/previews/V3/SCH.png" width="23%"/>](PWTS_designs/motor/previews/V3/SCH.png)
[<img src="PWTS_designs/motor/previews/V3/PCB.png" width="23%"/>](PWTS_designs/motor/previews/V3/PCB.png)
[<img src="PWTS_designs/motor/previews/V3/3D_Front.jpg" width="23%"/>](PWTS_designs/motor/previews/V3/3D_Front.jpg)
[<img src="PWTS_designs/motor/previews/V3/3D_Back.jpg" width="23%"/>](PWTS_designs/motor/previews/V3/3D_Back.jpg)

### [Ticker](PWTS_designs/ticker)

We're trying to design a "splitter" of sorts that alternates feeding two outputs.. a first iteration of working prototypes exists, but there's still work to be done with testing timings and use-cases.

[<img src="PWTS_designs/ticker/previews/SCH.png" width="23%"/>](PWTS_designs/ticker/previews/SCH.png)
[<img src="PWTS_designs/ticker/previews/PCB.png" width="23%"/>](PWTS_designs/ticker/previews/PCB.png)
[<img src="PWTS_designs/ticker/previews/3D_Front.png" width="23%"/>](PWTS_designs/ticker/previews/3D_Front.png)
[<img src="PWTS_designs/ticker/previews/3D_Back.png" width="23%"/>](PWTS_designs/ticker/previews/3D_Back.png)

