Zeners:
	https://www.mouser.dk/ProductDetail/onsemi-Fairchild/1N5231BTR?qs=SSucg2PyLi4jKyKWJywUyQ%3D%3D


Ultra:

	LED2:
		https://www.mouser.dk/ProductDetail/Kingbright/APFA3010LSEKJ3ZGKQBC?qs=ZqXcJfGlKsjQx6IyGYf3UQ%3D%3D

	R1 442 ohm:
		https://www.mouser.dk/ProductDetail/YAGEO/MFR-25FTE52-442R?qs=oAGoVhmvjhxoDvySPIxsTg%3D%3D
	R2 470 ohm:
		https://www.mouser.dk/ProductDetail/YAGEO/MFR-25FTE52-470R?qs=oAGoVhmvjhywqkRjln2gRw%3D%3D

	R3 620 ohm:
		https://www.mouser.dk/ProductDetail/YAGEO/MFR-25FTE52-620R?qs=UFD7vfw3J8pRhJGPsZN0Qg%3D%3D

Miller:
	D1:
		https://www.mouser.dk/ProductDetail/onsemi-Fairchild/1N4148?qs=i4Fj9T%2FoRm8RMUhj5DeFQg%3D%3D
	C1:
		https://www.mouser.dk/ProductDetail/KEMET/C410C474K5R5TA7200?qs=WLTup5mM6hiTb5nxgMGi%252BA%3D%3D
	FET:
		https://www.mouser.dk/ProductDetail/Microchip-Technology/VN3205N3-G?qs=M32N5aAaIfN9C45hhAlIdQ%3D%3D

